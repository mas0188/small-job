#pragma once
#include <string>

using namespace std;

// inspirace zde https://www.geeksforgeeks.org/introduction-to-binary-tree-data-structure-and-algorithm-tutorials/

/**
 * \file Node.hpp
 * \class Node
 * \brief Deklarace tridy Node
 * 
 * Datove cleny:
 * key = kodovany znak
 * value = znak
 * left = pointer na leveho potomka
 * right = pointer na pravaho potomka
 * 
 */

class Node
{
private:
    char key;
    char value;
    Node *left;
    Node *right;

public:
    Node(char key, char value);
    ~Node();
    char GetKey();
    char GetValue();
    Node *GetLeft();
    void SetLeft(Node *left);
    Node *GetRight();
    void SetRight(Node *right);
};

Node::Node(char key, char value)
{
    this->key = key;
    this->value = value;
    this->left = nullptr;
    this->right = nullptr;
}

char Node::GetKey()
{
    return this->key;
}

char Node::GetValue()
{
    return this->value;
}

Node *Node::GetLeft()
{
    return this->left;
}

void Node::SetLeft(Node *left)
{
    this->left = left;
}

Node *Node::GetRight()
{
    return this->right;
}

void Node::SetRight(Node *right)
{
    this->right = right;
}

/**
 * @brief Destrukor
 * 
 */

Node::~Node()
{
    delete this->right;
    this->right = nullptr;
    delete this->left;
    this->left = nullptr;
}
