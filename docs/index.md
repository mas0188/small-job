# Decoder

Purpose of this project was to create program which decodes morse code to text

## Commands

* `make`
* `./decoder < file.txt` - Decode morse code file
