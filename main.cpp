/**
 * @file main.cpp
 * @author Daniel Mašek MAS0188
 * @brief Hlavni kod
 * @version 1.1
 * @copyright Copyright (c) 2023
 * 
 */


#include <iostream>
#include <string>
#include "BinaryTree.hpp"

using namespace std;

/**
 * \brief Funkce decode
 * Funkce pro desifrovani kodovaneho textu
 * \param message kodovana zprava
 * \param tree slovnik
*/

void decode(string message, BinaryTree *tree)
{
    bool begining = true; // zacatek vety
    char oc;              // output char
    Node *currentnode(tree->GetRoot());
    for (int i = 0; i < message.length(); i++)
    {
        // cteni znaku
        if (message[i] == '.')
        {
            currentnode = currentnode->GetLeft();
        }
        else if (message[i] == '-')
        {
            currentnode = currentnode->GetRight();
        }

        if (message[i] == '/') // konec znaku
        {
            oc = currentnode->GetValue();
            begining ? oc -= 32 : oc;
            cout << oc;
            begining = false;
            currentnode = tree->GetRoot();
        }
        if (message[i] == '/' && message[i + 1] == '/' && message[i + 2] != '/') // konec slova
        {
            cout << " ";
            i++;
        }
        if (message[i] == '/' && message[i + 1] == '/' && message[i + 2] == '/') // konec vety
        {
            cout << ". ";
            begining = true;
            i += 2;
        }
    }
}

/**
* @brief Deklarace zpravy, nacteni zpravy do pameti, deklarace slovniku a spusteni dekodovani
* 
*/
int main()
{
    
    string coded;
    cout << "Insert coded text: " << endl;
    cin >> coded;

    // Deklarace binarniho stromu
    BinaryTree *tree = new BinaryTree();
    tree->SetRoot(new Node('/', '\0'));
    // 1. level
    tree->GetRoot()->SetLeft(new Node('.', 'e'));
    tree->GetRoot()->SetRight(new Node('-', 't'));
    // 2. level
    tree->GetRoot()->GetLeft()->SetLeft(new Node('.', 'i'));
    tree->GetRoot()->GetLeft()->SetRight(new Node('-', 'a'));
    tree->GetRoot()->GetRight()->SetLeft(new Node('.', 'n'));
    tree->GetRoot()->GetRight()->SetRight(new Node('-', 'm'));
    // 3. level
    tree->GetRoot()->GetLeft()->GetLeft()->SetLeft(new Node('.', 's'));
    tree->GetRoot()->GetLeft()->GetLeft()->SetRight(new Node('-', 'u'));
    tree->GetRoot()->GetLeft()->GetRight()->SetLeft(new Node('.', 'r'));
    tree->GetRoot()->GetLeft()->GetRight()->SetRight(new Node('-', 'w'));
    tree->GetRoot()->GetRight()->GetLeft()->SetLeft(new Node('.', 'd'));
    tree->GetRoot()->GetRight()->GetLeft()->SetRight(new Node('-', 'k'));
    tree->GetRoot()->GetRight()->GetRight()->SetLeft(new Node('.', 'g'));
    tree->GetRoot()->GetRight()->GetRight()->SetRight(new Node('-', 'o'));
    // 4. level
    tree->GetRoot()->GetLeft()->GetLeft()->GetLeft()->SetLeft(new Node('.', 'h'));
    tree->GetRoot()->GetLeft()->GetLeft()->GetLeft()->SetRight(new Node('-', 'v'));
    tree->GetRoot()->GetLeft()->GetLeft()->GetRight()->SetLeft(new Node('.', 'f'));
    tree->GetRoot()->GetLeft()->GetRight()->GetLeft()->SetLeft(new Node('.', 'l'));
    tree->GetRoot()->GetLeft()->GetRight()->GetRight()->SetLeft(new Node('.', 'p'));
    tree->GetRoot()->GetLeft()->GetRight()->GetRight()->SetRight(new Node('-', 'j'));
    tree->GetRoot()->GetRight()->GetLeft()->GetLeft()->SetLeft(new Node('.', 'b'));
    tree->GetRoot()->GetRight()->GetLeft()->GetLeft()->SetRight(new Node('-', 'x'));
    tree->GetRoot()->GetRight()->GetLeft()->GetRight()->SetLeft(new Node('.', 'c'));
    tree->GetRoot()->GetRight()->GetLeft()->GetRight()->SetRight(new Node('-', 'y'));
    tree->GetRoot()->GetRight()->GetRight()->GetLeft()->SetLeft(new Node('.', 'z'));
    tree->GetRoot()->GetRight()->GetRight()->GetLeft()->SetRight(new Node('-', 'q'));

    decode(coded, tree);
    cout << endl;

    //likvidace stromu
    delete tree;
    tree = nullptr;
    return 0;
}
