#pragma once
#include "Node.hpp"

using namespace std;

/**
 * \file BinaryTree.hpp
 * \class BinaryTree
 * \brief Deklarace tridy BinaryTree
 * 
 * Datove cleny:
 * root = zakladni bod
 * 
 * 
 */

class BinaryTree
{
private:
    Node *root;

public:
    BinaryTree();
    ~BinaryTree();
    Node *GetRoot();
    void SetRoot(Node *root);
};

BinaryTree::BinaryTree()
{
    this->root = nullptr;
}

Node *BinaryTree::GetRoot()
{
    return this->root;
}

void BinaryTree::SetRoot(Node *root)
{
    this->root = root;
}

BinaryTree::~BinaryTree()
{
    delete this->root;
    root = nullptr;
}
